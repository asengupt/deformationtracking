#ifndef DEFORMATIONTRACKING_DEFORMATIONTRACKING_H
#define DEFORMATIONTRACKING_DEFORMATIONTRACKING_H

#include <DeformationTracking/config.h>
#include <sofa/core/BehaviorModel.h>
#include <SofaUserInteraction/Controller.h>
#include <sofa/defaulttype/Vec.h>
#include <sofa/defaulttype/Quat.h>
#include <sofa/defaulttype/RigidTypes.h>
#include <sofa/defaulttype/VecTypes.h>
#include <list>
#include <iostream>
#include <sstream>
#include <vector>

#include <sofa/core/DataEngine.h>
#include <sofa/core/objectmodel/DataFileName.h>
#include <SofaUserInteraction/Controller.h>
#include <sofa/core/behavior/BaseController.h>
#include <sofa/simulation/Node.h>
#include <sofa/simulation/Simulation.h>
#include <sofa/defaulttype/SolidTypes.h>
#include <sofa/defaulttype/RigidTypes.h>
#include <sofa/defaulttype/Vec.h>
#include <sofa/helper/Quater.h>
#include <sofa/simulation/Node.h>
#include <cstring>

#include <SofaSimulationTree/GNode.h>

#include <sofa/core/objectmodel/MouseEvent.h>
#include <sofa/simulation/AnimateBeginEvent.h>

#include <NonRigidMatching.h>

#include <pcl/common/transforms.h>

#include <pcl/common/common.h>
#include <pcl/io/obj_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_lib_io.h>


#include <vtkPolyData.h>
#include <vtkPLYReader.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkVersion.h>
#include <vtkPointSource.h>
#include <vtkSphereSource.h>
#include <vtkOBBTree.h>
#include <vtkSliderWidget.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkCommand.h>
#include <vtkWidgetEvent.h>
#include <vtkCallbackCommand.h>
#include <vtkWidgetEventTranslator.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSliderWidget.h>
#include <vtkSliderRepresentation2D.h>
#include <vtkProperty.h>
#include <vtkMath.h>
#include <vtkPolygon.h>
#include <vtkCellArray.h>

#include<JacobianFEM.h>



namespace sofa
{

namespace component
{

namespace controller
{

using namespace sofa::defaulttype;
using core::objectmodel::Data;

/**
 * This BehaviorModel does nothing but contain a custom data widget.
 */
class DeformationTracking : public sofa::component::controller::Controller
{

public:
    SOFA_CLASS(DeformationTracking, sofa::component::controller::Controller);

    virtual void init();
    virtual void reinit();
    void updatePosition(double dt);
    virtual void track();
    virtual void initTracker();


    /****Data Variables****/
    Data< helper::vector<sofa::defaulttype::Vector3> > objectModel;
    Data< helper::vector<sofa::defaulttype::Vector3> > mechModel;

    Data< helper::vector<sofa::defaulttype::Vector3> > jacobianUp;
    Data< helper::vector<sofa::defaulttype::Vector3> > jacobianDown;

    Data< helper::vector<sofa::defaulttype::Vector4> > forcePoints;

    Data< helper::vector<float> > update_tracker;

    Data< std::string > objFileName;
    Data< std::string > mechFileName;
    Data< std::string > transformFileName;
    Data< std::string > dataFolder;
    Data< std::string > outputDirectory;
    Data< std::string > configPath;
    Data< std::string > caoModelPath;
    Data< float > C_x;
    Data< float > C_y;
    Data< float > F_x;
    Data< float > F_y;

    Data< float > iterations;

    Data< std::string > simulationMessage;
    Data< std::string > trackerMessage;
    /****Data Variables****/


    void draw(const sofa::core::visual::VisualParams* vparams);
    void handleEvent(core::objectmodel::Event *event);



protected:

    PolygonMesh model;
    PointCloud<PointXYZ>::Ptr cloud;
    PointCloud<PointXYZ>::Ptr mechanical_cloud;
    PointCloud<PointXYZRGB>::Ptr frame;
    Matrix4f transform;
    vector<PointXYZ> matched_points;
    vector<int> indices;
    Eigen::MatrixXd increment;



    RigidTracking rTrack;
    vpMbGenericTracker tracker;

    DeformationTracking();
    virtual ~DeformationTracking();	

    int counter;
    int pcd_count;
    NonRigidMatching nrm;
    vpHomogeneousMatrix cMo;
    JacobianFEM jFem;

    double current_residual;


    vector<Vector3d> x_; vector<Vector3d> _x;
    vector<Vector3d> y_; vector<Vector3d> _y;
    vector<Vector3d> z_; vector<Vector3d> _z;


    void incrementPcdCount();
    void matching();
    void matching_static();
    void update();
    void assimilate();
    void formatList(helper::vector<sofa::defaulttype::Vector3> simulationDump, vector<vector<Vector3d>> &unformatted_meshes, bool jacobian_mesh);



};


} // namespace behaviormodel

} // namespace component

} // namespace sofa


#endif 
