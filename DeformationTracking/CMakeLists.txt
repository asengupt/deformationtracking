cmake_minimum_required(VERSION 3.15 FATAL_ERROR)
project(DeformationTracking)

if(MSVC)
	set(CMAKE_CXX_FLAGS "-std=c++11")
else()
	set(CMAKE_CXX_FLAGS "-std=c++11 -m64")
endif()

#option(PD_LOCALE "Set the LC_NUMERIC number format to the default C locale" ON)

set(HEADER_FILES
    DeformationTracking.h
    OcclusionCheck.h
    config.h
    RigidTracking.h
    JacobianFEM.h
)

set(SOURCE_FILES
    DeformationTracking.cpp
    OcclusionCheck.cpp
    initDeformationTrackingPlugin.cpp 
    RigidTracking.cpp
    JacobianFEM.cpp
)

find_package(SofaFramework REQUIRED)
find_package(SofaBase REQUIRED)
find_package(SofaGeneral REQUIRED)
find_package(VISP REQUIRED)
find_package(Boost 1.57) 
find_package(PCL 1.9 REQUIRED COMPONENTS common io search visualization filters segmentation)
list(REMOVE_ITEM PCL_LIBRARIES "vtkproj4")

find_package(OpenCV REQUIRED)
find_package(VTK REQUIRED)

include(${VTK_USE_FILE})
include_directories(${PCL_INCLUDE_DIRS} ${CMAKE_CURRENT_SOURCE_DIR})
link_directories(${PCL_LIBRARY_DIRS})
include_directories(${VISP_INCLUDE_DIRS})

add_definitions(${PCL_DEFINITIONS})

add_library(${PROJECT_NAME} SHARED ${HEADER_FILES} ${SOURCE_FILES} NonRigidMatching.cpp OcclusionCheck.cpp)
add_library(matching NonRigidMatching.cpp OcclusionCheck.cpp)
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "-DSOFA_BUILD_DEFORMATIONTRACKING")

target_link_libraries(matching ${VISP_LIBRARIES} ${PCL_COMMON_LIBRARIES} ${PCL_IO_LIBRARIES} ${PCL_LIBRARIES} ${PCL_SEARCH_LIBRARIES} ${PCL_VISUALIZATION_LIBRARIES} ${PCL_SEGMENTATION_LIBRARIES} ${PCL_FILTERING_LIBRARIES} ${OpenCV_LIBS} ${VTK_LIBRARIES} ${VISP_LIBRARIES})

target_link_libraries(${PROJECT_NAME} SofaCore SofaSimulationCommon SofaUserInteraction ${VISP_LIBRARIES} ${PCL_LIBRARIES} ${PCL_COMMON_LIBRARIES} ${PCL_IO_LIBRARIES}  ${PCL_SEARCH_LIBRARIES} ${PCL_VISUALIZATION_LIBRARIES} ${PCL_SEGMENTATION_LIBRARIES} ${PCL_FILTERING_LIBRARIES} ${OpenCV_LIBS} ${VTK_LIBRARIES} ${VISP_LIBRARIES})

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/..")
target_include_directories(${PROJECT_NAME} PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>")
target_include_directories(${PROJECT_NAME} PUBLIC "$<BUILD_INTERFACE:${PCL_INCLUDE_DIRS}>")

install(TARGETS ${PROJECT_NAME}
        DESTINATION include/${PROJECT_NAME}
        COMPONENT DeformationTracking_libraries
        EXPORT DeformationTracking
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)


add_custom_command(TARGET ${PROJECT_NAME} PRE_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy_directory
                       ${CMAKE_CURRENT_SOURCE_DIR}/data/ ${CMAKE_BINARY_DIR}/data/)

